
package au.edu.uts.aip.enrollment;


import au.edu.uts.aip.enrollment.mode.DataStoreException;
import au.edu.uts.aip.enrollment.mode.EnrollmentDAO;
import au.edu.uts.aip.enrollment.mode.EnrollmentDTO;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;




@Named
@RequestScoped
public class EnrollmentController{
    private String studentname;
    private EnrollmentDTO enrollment = new EnrollmentDTO();
   
   
    /*
    EnrollmentDTO constructor
    */    
    public EnrollmentDTO getEnrollment() {
        return enrollment;
    }
   /*
    Log in with security container. If sucess, page turns to "welcome" page. If not, turn to "index" page. 
    */
    public String login() throws DataStoreException {
      
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        try {
              request.login(enrollment.getStudentid(), enrollment.getPassword());
            } catch (ServletException e) {
             context.addMessage(null, new FacesMessage(e.getMessage()));
             return  "index";
             }
            return "welcome";
           }
   
    /*
    get all enrollment records in table
    */
     public ArrayList<EnrollmentDTO> getAllEnrollments() throws DataStoreException {
        EnrollmentDAO dao;
        dao = new EnrollmentDAO();
        ArrayList<EnrollmentDTO> enrollments=dao.list();
        return enrollments;
    }
     /*
     get  record with identified student ID
     */
    public EnrollmentDTO getEnrollment(String studentid) throws DataStoreException {
        EnrollmentDAO dao;
        dao = new EnrollmentDAO();
        return dao.get(studentid);
    }
    
    public void init(String studentid) throws DataStoreException {
        EnrollmentDAO dao;
        dao = new EnrollmentDAO();
        enrollment=dao.get(studentid);
    }
    
    public void loadEnrollment(String studentid) throws DataStoreException{
         EnrollmentDAO dao;
        dao = new EnrollmentDAO();
        enrollment=dao.get(studentid);
    }
    /*
    delete enrollment
    */
    public String deleteEnrollment() throws DataStoreException{
         EnrollmentDAO dao;
         dao= new EnrollmentDAO();
         dao.delete(enrollment);
        return "enrollment?faces-redirect=true";
    }
    /*
    Save the creating new recod
    */
     public String saveAsNew() throws DataStoreException {
        EnrollmentDAO dao=new EnrollmentDAO();
        dao.create(enrollment);
        return "enrollment?faces-redirect=true";
    }
     /*
     Save the changes in record
     */
      public String saveChanges() throws DataStoreException {
        EnrollmentDAO dao=new EnrollmentDAO();
        dao.update(enrollment);
        return "enrollment?faces-redirect=true";
    }
     
      /*
      Log out with security container
      */
    public String logout() throws ServletException {
        
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        try {
              request.logout();
        } catch (ServletException e) {
    
                context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return "index";
    }
    
   /* private static HttpServletRequest getRequest() {
        FacesContext context = FacesContext.getCurrentInstance();
        return (HttpServletRequest)context.getExternalContext().getRequest();
    }
    
    private void showError(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }
    */
}
