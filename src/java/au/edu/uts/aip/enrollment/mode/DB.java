/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.edu.uts.aip.enrollment.mode;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author
 */
public class DB implements AutoCloseable {
    private Connection conn;
    
    /**
     * Creates a new DB.
     * 
     * @throws DataStoreException 
     */
    public DB() throws DataStoreException {
        try {
            DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
            conn = ds.getConnection();
        } catch (NamingException|SQLException e) {
            throw new DataStoreException(e);
        }
    }
    
    /**
     * Performs an SQL query as a prepared statement using the supplied arguments
     * and returns a result set.
     * 
     * @param sql the SQL query to execute
     * @param arguments parameters for the prepared statement
     * @return a result set for the query
     * @throws DataStoreException 
     */
    public ResultSet query(String sql, Object... arguments) throws DataStoreException {
        try {
            PreparedStatement stmt = prepare(sql, arguments);
            ResultSet rs = stmt.executeQuery();
        return rs;
        } catch (SQLException e) {
            throw new DataStoreException(e);
        }
    }
    
    /**
     * Performs an SQL query as a prepared statement using the supplied arguments.
     * No result set is returned.
     * 
     * @param sql the SQL query to execute
     * @param arguments parameters for the prepared statement
     * @throws DataStoreException 
     */
    public void execute(String sql, Object... arguments) throws DataStoreException {
        try {
            PreparedStatement stmt = prepare(sql, arguments);
            stmt.execute();
        }
        catch (SQLException e) {
            throw new DataStoreException(e);
        }
    }
    
    /**
     * A helper method to generate a PreparedStatement and feed in its arguments.
     * It understands only String, Integer and java.util.Date arguments, 
     * @param sql the SQL query to use for the prepared statement
     * @param arguments the arguments to feed into the prepared statement
     * @return the newly generated prepared statement
     * @throws DataStoreException 
     */
    private PreparedStatement prepare(String sql, Object... arguments) throws DataStoreException {
        try {
            for (Object o : arguments) {
                System.out.println(o);
            }
            PreparedStatement stmt = conn.prepareStatement(sql);
            for (int i = 0; i < arguments.length; i++) {
             
                 stmt.setString(i+1, (String)arguments[i]);
                
            }
            return stmt;
        }
        catch (SQLException e) {
            throw new DataStoreException(e);
        }
    }
/*
    close connection, when we do not use databsae
    */
    @Override
    public void close() throws DataStoreException {
        try {
            conn.close();
        } catch (SQLException e) {
            throw new DataStoreException(e);
        }
    }
    
}
