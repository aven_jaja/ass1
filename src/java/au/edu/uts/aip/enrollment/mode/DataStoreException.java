
package au.edu.uts.aip.enrollment.mode;
/*
when there is something wrong with the statement, message will show.
*/

public class DataStoreException extends Exception {
    
    public DataStoreException(String message) {
        super(message);
    }
    
    public DataStoreException(Throwable cause) {
        super(cause);
    }
    
    public DataStoreException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
