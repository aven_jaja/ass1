/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.edu.uts.aip.enrollment.mode;



import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;



/**
 *
 * @author Xin Li
 */
public class EnrollmentDAO implements Serializable{
    /*
    Identify the SQL query to String variables 
    */
  
    private static final String CREATE_QUERY = "insert into enrollment (studentid, studentname,password, courseid, coursename, classtime, classroom) values (?, ?, ?, ?, ?, ?, ?)";
    private static final String GET_QUERY = "select studentid, studentname, password, courseid, coursename, classtime, classroom  from enrollment where studentid = ?";
    private static final String LIST_QUERY = "select studentid, studentname, password,courseid, coursename, classtime, classroom from enrollment";
    private static final String UPDATE_QUERY = "update enrollment set studentname=?, password=?,courseid = ?, coursename = ?, classtime = ?, classroom = ? where studentid = ?";
    private static final String DELETE_QUERY = "delete from enrollment where studentid = ?";

      private EnrollmentDTO createRowDTO(ResultSet rs) throws SQLException {
        EnrollmentDTO result = new EnrollmentDTO();
        result.setStudentname(rs.getString("studentname"));
        result.setStudentid(rs.getString("studentid"));
        result.setPassword(rs.getString("password"));
        result.setCoursename(rs.getString("coursename"));
        result.setCourseid(rs.getString("courseid"));
        result.setClasstime(rs.getString("classtime"));
        result.setClassroom(rs.getString("classroom"));
        return result;
    }
 
    /*
      method create is to insert a new record to table.The method aapply the class DB, and execute method of the DB class.
      */
    public void create(EnrollmentDTO enrollment) throws DataStoreException {
        try (DB db = new DB()) {
            db.execute(CREATE_QUERY, enrollment.getStudentid(), enrollment.getStudentname(), enrollment.getPassword(),enrollment.getCourseid(), enrollment.getCoursename(),enrollment.getClasstime(), enrollment.getClassroom());
        }
    }
    
    
 /*
    Search record with identified student ID  
    */
    public EnrollmentDTO get(String studentid) throws DataStoreException {
        try (DB db = new DB()) {
            ResultSet rs = db.query(GET_QUERY, studentid);
            if (rs.next())
                return createRowDTO(rs);
            return null;
        }
        catch (SQLException e) {
            throw new DataStoreException(e);
        }
    }
/*
    List all the records in tabele enrollment. Records are stored in Array.
    */
  
    public ArrayList<EnrollmentDTO> list() throws DataStoreException {
        try (DB db = new DB()) {
            ResultSet rs = db.query(LIST_QUERY);
            ArrayList<EnrollmentDTO> enrollments = new ArrayList<>();
            while (rs.next()) {
                enrollments.add(createRowDTO(rs));
            }
            return enrollments;
        }
        catch (SQLException e) {
            throw new DataStoreException(e);
        }
    }
/*
    change the attribute values of one record. 
    */

    public void update(EnrollmentDTO enrollment) throws DataStoreException {
        try (DB db = new DB()) {
            db.execute(UPDATE_QUERY, enrollment.getStudentname(), enrollment.getPassword(), enrollment.getCourseid(), enrollment.getCoursename(), enrollment.getClasstime(), enrollment.getClassroom(),  enrollment.getStudentid());
        }
    }

  /*
    Delete one record
    */
    public void delete(EnrollmentDTO enrollment) throws DataStoreException {
        try (DB db = new DB()) {
            db.execute(DELETE_QUERY, enrollment.getStudentid());
        }
    }
   
}
