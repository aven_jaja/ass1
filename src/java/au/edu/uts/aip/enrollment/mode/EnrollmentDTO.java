/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.edu.uts.aip.enrollment.mode;

import java.io.Serializable;

/**
 *
 * @author 
 */
public class EnrollmentDTO implements Serializable{
 /**
 *
 * set variables
 */
    private String studentname;
    private String studentid;
    private String password;
    private String coursename;
    private String courseid;
    private String classtime;
    private String classroom;
    
 /**
 *
 * set values of variables
     * @param studentname
   
 */
    
    public void  setStudentname(String studentname){
                this.studentname=studentname;
    }
    
    public void setStudentid(String studentid){
                this.studentid=studentid;
    }
    
    public void setPassword(String password){
                this.password=password;
    }
    
    public void setCoursename(String coursename){
                this.coursename=coursename;
    }
    
     public void setCourseid(String courseid){
                 this.courseid=courseid;
    }
     
    public void setClasstime(String classTime){
                this.classtime=classTime;
    }
    
     public void setClassroom(String classRoom){
                 this.classroom=classRoom;
    }
     
 /**
 *
 * get values of variable
     * @return 
 */
     public String getStudentname(){
            return studentname;
     }
     
     public String getStudentid(){
            return studentid;
     }
     
     public String getPassword(){
            return password;
     }
     
     public String getCoursename(){
            return coursename;
     }
     
    public String getCourseid(){
           return courseid;
     } 
    
    public String getClassroom(){
           return classroom;
     }
    
    public String getClasstime(){
           return classtime;
     }
}
