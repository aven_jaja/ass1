/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.edu.uts.aip.enrollment.mode;

/**
 *
 * @author Ryan station
 */
public class RegisterDAO {
    
    private static final String CREATE_QUERY = "insert into enrollment (studentid, studentname,password) values (?, ?, ?)";
    private static final String GET_QUERY = "select studentid, studentname, password, courseid, coursename, classtime, classroom  from enrollment where studentid = ?";
    private static final String LIST_QUERY = "select studentid, studentname, password,courseid, coursename, classtime, classroom from enrollment";
    private static final String UPDATE_QUERY = "update enrollment set studentname=?, password=?,courseid = ?, coursename = ?, classtime = ?, classroom = ? where studentid = ?";
    private static final String DELETE_QUERY = "delete from enrollment where studentid = ?";
    
}
