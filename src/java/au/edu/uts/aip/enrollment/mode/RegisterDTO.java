/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.edu.uts.aip.enrollment.mode;

/**
 *
 * @author
 */
public class RegisterDTO {
    
    private String studentname;
    private String studentid;
    private String password;
    
    public void setStudentname(String studentname){
           this.studentname=studentname;    
    }
    
    public void setStudentid(String studentid){
           this.studentid=studentid;
    }
    
    public void setPassword(String password){
            this.password=password;
    }
    
    public String getStudentname(){
            return studentname;
    }
    public String getStudentid(){
           return studentid;
    }
    public String getPassword(){
           return password;
    }
    
    
}
